  
CLIENT_KEYSTORE_DIR=../Client/src/main/resources
SERVER_KEYSTORE_DIR=../Server/src/main/resources
CLIENT_KEYSTORE=$CLIENT_KEYSTORE_DIR/client.p12
SERVER_KEYSTORE=$SERVER_KEYSTORE_DIR/server.p12
JAVA_CA_CERTS=$JAVA_HOME/jre/lib/security/cacerts

# Generate a client and server RSA 2048 key pair
keytool -genkeypair -alias client -keyalg RSA -keysize 2048 -storetype PKCS12 -dname "CN=AdrianS,OU=AdrianS,O=None,L=Poland,S=PL,C=U" -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keypass password -keystore $CLIENT_KEYSTORE -storepass password
keytool -genkeypair -alias server -keyalg RSA -keysize 2048 -storetype PKCS12 -dname "CN=AdrianS,OU=AdrianS,O=None,L=Poland,S=PL,C=U" -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keypass password -keystore $SERVER_KEYSTORE -storepass password

# Export public certificates for both the client and server
keytool -exportcert -alias client -file myClient-public.cer -keystore $CLIENT_KEYSTORE -storepass password
keytool -exportcert -alias server -file myServer-public.cer -keystore $SERVER_KEYSTORE -storepass password

# Import the client and server public certificates into each others keystore
keytool -importcert -keystore $CLIENT_KEYSTORE -alias server-public-cert -file myServer-public.cer -storepass password -noprompt
keytool -importcert -keystore $SERVER_KEYSTORE -alias client-public-cert -file myClient-public.cer -storepass password -noprompt