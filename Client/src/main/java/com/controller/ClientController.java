package com.controller;


import com.model.Client;
import com.model.User;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;


@Controller
@RequestMapping("")
public class ClientController {

    private static final String URL = "https://localhost:8111/confirm_access";
    private static final String TOKEN = "https://localhost:8111/token";
    private static final String RESOURCE = "https://localhost:8111/get_protected_data";

    private RestTemplate restTemplate;
    private User user;

    @Autowired
    ClientController(RestTemplate restTemplate, User user){
        this.restTemplate = restTemplate;
        this.user = user;
    }



    @GetMapping("/client")
    public ModelAndView showForm(Model theModel){
        Client client = new Client();
        theModel.addAttribute(client);
        return new ModelAndView("form");
    }

    @PostMapping("/confirm_access")
    public ModelAndView submitForm(@ModelAttribute("client") Client theClient){

        UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(URL);
        urlBuilder.queryParam("client_id", theClient.getClientId());
        urlBuilder.queryParam("redirect_uri", theClient.getRedirectUri());
        urlBuilder.queryParam("scope", theClient.getScope());
        urlBuilder.queryParam("response_type", theClient.getResponseType());
        urlBuilder.queryParam("client_secret", theClient.getClientSecret());
        String url = urlBuilder.build(false).encode().toUriString();
        return new ModelAndView("redirect:" + url);
    }

    @GetMapping("/show_data")
    public ModelAndView confirmAccess(@RequestParam(required = true)String code, Model theModel) {

        String encodedCredentials = new String(Base64.encodeBase64(user.credentials().getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Basic " + encodedCredentials);
        HttpEntity<String> request = new HttpEntity<>(headers);
        ResponseEntity<String> response = restTemplate.exchange((TOKEN + "?code=" + code), HttpMethod.GET, request, String.class);
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers2.add("Authorization", "Bearer " + response.getBody());
        HttpEntity<String> entity = new HttpEntity<>(headers2);
        ResponseEntity<String> retrievedData = restTemplate.exchange(RESOURCE, HttpMethod.GET, entity, String.class);
        theModel.addAttribute("protectedData", retrievedData.getBody());
        return new ModelAndView("ViewProtectedData");
    }
}


