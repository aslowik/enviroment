package com;


import com.model.User;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;



@SpringBootApplication
public class ClientApp {

    @Value("${server.ssl.key-password}")
    private String keyPassword;
    @Value("client.p12")
    private ClassPathResource classPathResource;
    @Value("#{ @environment['configuration.baseUrl'] }")
    private String serverBaseUrl;


    public static void main(String[] args) {
        SpringApplication.run(ClientApp.class, args);
    }

    @Bean
    public User init() {
       return new User("admin","admin","resourceOwner");
    }


    @Bean
    protected RestTemplate restTemplate() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        KeyStore myKeyStore = KeyStore.getInstance("PKCS12");
        InputStream inputStream = classPathResource.getInputStream();
        myKeyStore.load(inputStream, keyPassword.toCharArray());
        SSLContext sslcontext = new SSLContextBuilder()
                .loadTrustMaterial(new TrustSelfSignedStrategy())
                .loadKeyMaterial(myKeyStore, keyPassword.toCharArray()).build();
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslcontext);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(socketFactory).build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(factory);
    }

}
