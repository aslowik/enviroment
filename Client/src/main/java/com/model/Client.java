package com.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Client {

    private String clientId;
    private String redirectUri;
    private String scope;
    private String responseType;
    private Boolean isAuthorized;
    private String clientSecret;

}

