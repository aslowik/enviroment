
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
<script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script>
<script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script>
<head><script src="chrome-extension://mooikfkahbdckldjjndioackbalphokd/assets/prompt.js">

</script>
</head>
<body>
<h1>Oauth client form</h1>
<p>Fill all fields (or leave defaults)</p>

<sf:form action="confirm_access" modelAttribute="client" method="post">

    <sf:input path="responseType" id="response_type" value="code"/><label>   - Response type</label><br>

    <sf:input path="clientId" id="client_id" value="adrian"/><label>   - Client ID</label><br>

    <sf:input path="redirectUri" id="redirect_uri" value="https://localhost:8222/show_data"/><label>   - Redirect Uri</label><br>

    <sf:input path="scope" id="scope" value="read"/><label>   - Scope</label><br>

    <sf:input path="clientSecret" id="clientSecret" value="secret"/><label>   - Client Secret</label><br><br>

    <button type="submit" class="btn btn-primary"><s:message text="Submit"/></button>
</sf:form>
</body>
</html>