package com.controller;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ClientControllerTest {

    @Autowired
    RestTemplate restTemplate;

    private static final String TOKEN = "https://localhost:8111/token";
    private static final String RESOURCE = "https://localhost:8111/get_protected_data";

    @Test
    public void givenRightAuthCodeAndToken_whenGetSecureRequest_thenReceiveProtectedData(){
        //Given
        String rightCode = "11111";
        String rightCredentials = "admin:admin";
        String encodedCredentials = new String(Base64.encodeBase64(rightCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + encodedCredentials);
        HttpEntity<String> request = new HttpEntity<>(headers);
        String rightTokenCode = "12345";
        HttpHeaders headers2 = new HttpHeaders();
        headers2.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers2.add("Authorization", "Bearer " + rightTokenCode);
        HttpEntity<String> entity = new HttpEntity<>(headers2);
        //when
        ResponseEntity<String> response = restTemplate.exchange((TOKEN + "?code=" + rightCode), HttpMethod.GET, request, String.class);
        ResponseEntity<String> retrievedData = restTemplate.exchange(RESOURCE, HttpMethod.GET, entity, String.class);
        //then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(HttpStatus.OK, retrievedData.getStatusCode());
    }
}
