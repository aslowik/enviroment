

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<html>
<script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/prompt.js"></script>
<script src="chrome-extension://ljdobmomdgdljniojadhoplhkpialdid/page/runScript.js"></script>
<head><script src="chrome-extension://mooikfkahbdckldjjndioackbalphokd/assets/prompt.js">

</script>
</head>
<body>
<h1>OAuth Approval</h1>
<p>Do you authorize ${ClientId} to access your protected resources?</p>
<sf:form id="confirmationForm" name="confirmationForm" action="authorize" method="post" modelAttribute="client">


                    <label class="radio">
                        <sf:radiobutton path="isAuthorized" id="gridRadios1" value="true" checked="checked"/>
                        <span><b>Approve</b></span></label>


               <label class="radio">
                    <sf:radiobutton path="isAuthorized" id="gridRadios2" value="false" />
                    <span><b>Deny</b></span>
                </label>


<%--    <input name="user_oauth_approval" value="true" type="hidden">--%>
<%--    <ul><li>--%>
<%--        <div class="form-group">scope.read: <input type="radio" name="scope.read" value="true">Approve <input type="radio" name="scope.read" value="false" checked="">Deny</div>--%>
<%--    </li></ul><label>--%>
<%--    <input name="authorize" value="Authorize" type="submit"></label>--%>
    <button type="submit" class="btn btn-primary"><s:message text="submit"/></button>


    <sf:hidden path="responseType"/>
    <sf:hidden path="clientId"/>
    <sf:hidden path="redirectUri"/>
    <sf:hidden path="scope"/>
    <sf:hidden path="clientSecret"/>
</sf:form></body></html>