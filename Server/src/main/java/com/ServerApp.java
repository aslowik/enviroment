package com;


import com.model.AuthorizationCode;
import com.model.Client;
import com.model.TokenCode;
import com.model.User;
import com.repository.AppRepository;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;


@SpringBootApplication
public class ServerApp {

    @Value("${server.ssl.key-password}")
    private String keyPassword;
    @Value("server.p12")
    private ClassPathResource classPathResource;

    @Autowired
    private AppRepository appRepository;

    public static void main(String[] args) {
        SpringApplication.run(ServerApp.class, args);
    }

    @PostConstruct
    public void init() {
        appRepository.addClient(new Client("adrian","https://localhost:8222/show_data","read","code", false, "secret"));
        appRepository.addUser(new User("admin","{noop}admin","resourceOwner"));
        appRepository.addData("this is protected data retrieved with authentication code flow");
        //below for integration testing purposes
        appRepository.addCode(new AuthorizationCode("11111","testClient","scope","uri",null));
        appRepository.addToken(new TokenCode("12345", null));
    }


    @Bean
    protected RestTemplate restTemplate() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        KeyStore myKeyStore = KeyStore.getInstance("PKCS12");
        InputStream inputStream = classPathResource.getInputStream();
        myKeyStore.load(inputStream, keyPassword.toCharArray());
        SSLContext sslcontext = new SSLContextBuilder()
                .loadTrustMaterial(new TrustSelfSignedStrategy())
                .loadKeyMaterial(myKeyStore, keyPassword.toCharArray()).build();
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslcontext);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(socketFactory).build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);
        return new RestTemplate(factory);
    }
}

