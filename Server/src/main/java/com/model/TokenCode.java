package com.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
public class TokenCode {

    private String code;
    private LocalDateTime expirationDate;

}
