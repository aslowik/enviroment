package com.model;


import lombok.Data;
import lombok.AllArgsConstructor;

@Data
@AllArgsConstructor
public class Client {

    private String clientId;
    private String redirectUri;
    private String scope;
    private String responseType;
    private Boolean isAuthorized;
    private String clientSecret;
}

