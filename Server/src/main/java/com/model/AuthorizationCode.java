package com.model;

import lombok.Data;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class AuthorizationCode {

    private String code;
    private String clientId;
    private String approvedScopes;
    private String redirectUri;
    private LocalDateTime expirationDate;

}
