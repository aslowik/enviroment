package com.controller;

import com.model.AuthorizationCode;
import com.model.Client;
import com.repository.AppRepository;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.Objects;

@RequestMapping("")
@Controller
public class ServerController {


    private final AppRepository appRepository;

    @Autowired
    ServerController(AppRepository appRepository){
        this.appRepository = appRepository;
    }

    @GetMapping("/confirm_access")
    public ModelAndView confirmAccess(@RequestParam Map<String,String> requestParams, Model model) {
        Client client = appRepository.createClientWithParams(requestParams);
        if (appRepository.checkIfRepoContainsClient(client)){
            model.addAttribute("ClientId", requestParams.get("client_id"));
            model.addAttribute("client", client);
            return new ModelAndView("Approval");
        }else{
            return new ModelAndView("Error");
        }
    }

    @PostMapping("/authorize")
    public ModelAndView authorize(@ModelAttribute("client")Client client){
        if (client.getIsAuthorized()){
            AuthorizationCode code = appRepository.createAndRegisterCode(client);
            UriComponentsBuilder redirectUri = UriComponentsBuilder
                    .fromHttpUrl(appRepository.returnRedirectUriByClientName(client));
            redirectUri.queryParam("code", code.getCode());
            String url = redirectUri.build(false).encode().toUriString();
            return new ModelAndView("redirect:" + url, HttpStatus.OK);
        }else {
            return new ModelAndView("Error",HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/token")
    public ResponseEntity token(@RequestHeader HttpHeaders headers, @RequestParam String code){
        String header = Objects.requireNonNull(headers.getFirst("Authorization")).replace("Basic ","");
        String decodedCredentials = new String (Base64.decodeBase64(header.getBytes()));
        if (appRepository.checkIfRepoContainsCode(code) && appRepository.checkIfRepoContainsCredentials(decodedCredentials)){
            String tokenCode = appRepository.createAndRegisterToken();
            return new ResponseEntity<>(tokenCode, HttpStatus.OK);
        }else{
            return new ResponseEntity<>("wrong credentials", HttpStatus.UNAUTHORIZED);
        }
    }

    //protected resources
    @GetMapping("/get_protected_data")
    public ResponseEntity getProtectedData(@RequestHeader HttpHeaders headers){
        String retrievedToken = Objects.requireNonNull(headers.getFirst("Authorization")).replace("Bearer ","");
        if(appRepository.checkIfRepoContainsToken(retrievedToken)){
            return new ResponseEntity<>(appRepository.getProtectedData(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("wrong token", HttpStatus.UNAUTHORIZED);
        }
    }

}

