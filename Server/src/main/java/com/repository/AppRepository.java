package com.repository;

import com.model.AuthorizationCode;
import com.model.Client;
import com.model.TokenCode;
import com.model.User;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class AppRepository {


    private List<Client> clients = new ArrayList<>();
    private List<AuthorizationCode> codes = new ArrayList<>();
    private List<TokenCode> tokens = new ArrayList<>();
    private List<String> protectedData = new ArrayList<>();
    private List<User> users = new ArrayList<>();

    //Client service
    public void addClient(Client client){
        this.clients.add(client);
    }

    public Client createClientWithParams(Map<String,String> requestParams){
        return new Client(
                requestParams.get("client_id"),
                requestParams.get("redirect_uri"),
                requestParams.get("scope"),
                requestParams.get("response_type"),
                false,
                requestParams.get("client_secret"));
    }

    public boolean checkIfRepoContainsClient(Client client){
        return this.clients.contains(client);
    }

    public String returnRedirectUriByClientName(Client client){
        for (Client value : clients) {
            if (value.getClientId().equals(client.getClientId())) {
                return value.getRedirectUri();
            }
        }
        return null;
    }

    //User service
    public void addUser(User user){
        this.users.add(user);
    }

    public String getUserPassword(){
        return users.get(0).getPassword();
    }

    public String getUserLogin(){return users.get(0).getLogin();}

    public boolean checkIfRepoContainsCredentials(String credentials){
        String[] split = credentials.split(":");
        for (User user : users) {
            if (user.getLogin().equals(split[0]) && user.getPassword().replace("{noop}", "").equals(split[1]))
                    return true;
            }
        return false;
    }

    //Code service
    public void addCode(AuthorizationCode code){
        this.codes.add(code);
    }

    public boolean checkIfRepoContainsCode(String code){

        for (AuthorizationCode authorizationCode : codes) {
            if (authorizationCode.getCode().equals(code)) {
                return true;
            }
        }
        return false;
    }

    public AuthorizationCode createAndRegisterCode(Client client){
        AuthorizationCode code = new AuthorizationCode("12345", client.getClientId(),
                client.getScope(),
                client.getRedirectUri(),
                LocalDateTime.now().plusMinutes(5));
        this.codes.add(code);
        return code;
    }

    //Token service
    public void addToken(TokenCode token){
        this.tokens.add(token);
    }

    public String createAndRegisterToken(){
        final SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        Base64.encodeBase64String(randomBytes);
        TokenCode token = new TokenCode(Base64.encodeBase64String(randomBytes),LocalDateTime.now().plusMinutes(30));
                tokens.add(token);
                return token.getCode();

        }

    public boolean checkIfRepoContainsToken(String token){
        for (TokenCode tokenCode : tokens) {
            if (tokenCode.getCode().equals(token)) {
                return true;
            }
        }
        return false;
    }

    //Protected data service
    public void addData(String data){this.protectedData.add(data);}

    public String getProtectedData(){
        return this.protectedData.toString();
    }



}
