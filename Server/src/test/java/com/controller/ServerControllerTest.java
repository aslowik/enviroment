package com.controller;

import com.model.AuthorizationCode;
import com.model.Client;
import com.model.TokenCode;
import com.model.User;
import com.repository.AppRepository;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ServerControllerTest {

    private MockMvc mockMvc;

    @Autowired
    AppRepository appRepository;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        User user = new User("login","pass","role");
        Client client = new Client("client","https://localhost:8111", "scope", "type",true, "secret");
        AuthorizationCode code = new AuthorizationCode("12345","client","scope", "uri", null);
        TokenCode token = new TokenCode("12345",null);
        appRepository.addUser(user);
        appRepository.addClient(client);
        appRepository.addCode(code);
        appRepository.addToken(token);
    }

    //Endpoint: /get_protected_data

    @Test
    public void givenRightToken_whenAccessingResourceEndpoint_thenStatusOk() throws Exception{
        TokenCode token = new TokenCode("12345", null);
        mockMvc.perform(get("/get_protected_data")
                .header("Authorization","Bearer "+ token.getCode()))
                .andExpect(status().isOk());
    }

    @Test
    public void givenWrongToken_whenAccessingResourceEndpoint_thenStatusUnauthorized() throws Exception {
        TokenCode token = new TokenCode("wrongToken", null);
        mockMvc.perform(get("/get_protected_data")
                .header("Authorization","Bearer "+ token))
                .andExpect(status().isUnauthorized());
    }

    //Endpoint: /token

    @Test
    public void givenRightHeaderAndAuthCode_whenAccessingTokenEndpoint_thenStatusOk() throws Exception {
        String code = "12345";
        String credentials = "login:pass";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        mockMvc.perform(get("/token")
                .header("Authorization", "Basic "+encodedCredentials)
                .param("code", code))
                .andExpect(status().isOk());
    }

    @Test
    public void givenWrongAuthCode_whenAccessingTokenEndpoint_thenStatusUnauthorized() throws Exception {
        String code = "wrongToken";
        String credentials = "login:pass";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        mockMvc.perform(get("/token")
                .header("Authorization", "Basic "+encodedCredentials)
                .param("code", code))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void givenWrongHeader_whenAccessingTokenEndpoint_thenStatusUnauthorized() throws Exception {
        String code = "12345";
        String credentials = "login:wrongPass";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        mockMvc.perform(get("/token")
                .header("Authorization", "Basic "+encodedCredentials)
                .param("code", code))
                .andExpect(status().isUnauthorized());
    }

    //Endpoint: /authorize

    @Test
    public void givenRightModel_whenAccessingAuthorizeEndpoint_thenRightRedirectUrl() throws Exception {
        Client rightModel = new Client("client","https://localhost:8111", "scope", "type",true, "secret");
        mockMvc.perform(
                post("/authorize").flashAttr("client", rightModel))
                .andExpect(redirectedUrl("https://localhost:8111?code=12345"));

    }

    @Test
    public void givenWrongModel_whenAccessingAuthorizeEndpoint_thenStatusUnauthorized() throws Exception {
        Client wrongModel = new Client("notRegisteredClient","https://localhost:8111", "scope", "type",false, "secret");
        mockMvc.perform(
                post("/authorize").flashAttr("client", wrongModel))
                .andExpect(status().isUnauthorized());
    }

}
